# SimpleNotes

Create your notes and lists in a sterling word processor that unites both simplicity and usability. 
The app allows you to take your notes quicker and more informative with sketches. 
GeoNotes provides fast access to your drive to attach photos to your notes. 

