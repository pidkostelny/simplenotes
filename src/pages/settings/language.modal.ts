import {Component, OnInit} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";
import {HomePage} from "../home/home";
import {Settings, SETTINGS_KEY} from "../../shared/setings";
import {Storage} from "@ionic/storage";

@Component({
  template: `
    <ion-header>
      <ion-toolbar>
        <button ion-button menuToggle>
          <ion-icon name="menu"></ion-icon>
        </button>
        <!--<ion-buttons start>-->
          <!--<button ion-button (click)="dismiss()">-->
            <!--<span ion-text color="primary" showWhen="ios">Cancel</span>-->
            <!--<ion-icon name="md-close" showWhen="android,windows"></ion-icon>-->
          <!--</button>-->
        <!--</ion-buttons>-->
        <ion-title>
          {{'LANGUAGE' | translate}}
        </ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button ion-item *ngFor="let lang of languages" (click)="setLanguage(lang)">
          {{lang.name}}
        </button>
      </ion-list>
    </ion-content>
    `
})
export class LanguageModal implements OnInit {

  languages: { name: string, keyWord: string}[];

  settings;

  constructor(private translate: TranslateService,
              private viewCtrl: ViewController,
              private navCtrl: NavController,
              private storage: Storage) {
    this.languages = [{name:"English", keyWord: "en-US"}, { name:"Українська", keyWord : "uk-UA"}];
  }

  // setLanguage(language) {
  //   this.translate.setDefaultLang(language.keyWord);
  //   this.viewCtrl.dismiss(language.keyWord);
  // }

  dismiss() {
    this.viewCtrl.dismiss();
  }


  //Temporary

  ngOnInit() {
    this.storage.get(SETTINGS_KEY).then(settings =>
      this.settings = settings ? settings : new Settings());
  }

  setLanguage(language) {
      this.translate.setDefaultLang(language.keyWord);
      this.settings.language = language.keyWord;
      this.saveSettings();
      this.navCtrl.setRoot(HomePage);
    }

  private saveSettings() {
    this.storage.set(SETTINGS_KEY, this.settings);
  }

}
