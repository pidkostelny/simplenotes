import {AfterViewInit, Component, OnInit} from '@angular/core';

import {
  ActionSheetController, Ion, NavController, Platform, Popover, PopoverController
} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";

import {NotePage} from "../note/note";
import {NoteService} from "../../shared/services/note.service";
import {Note} from "../../shared/classes/Note";
import {ListOptionsPopover} from "./list-options.popover";
import {ItemOptionsPopover} from "./item-options.popover";
import {Storage} from "@ionic/storage";
import {NotificationPage} from "../notification/notification";
import {SplashScreen} from "@ionic-native/splash-screen";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  notes: any[] = [];

  selected = 0;
  isEnableSelectingMode: boolean = false;
  popover: Popover;

  itemOptionsPopover: Popover;

  constructor(public navCtrl: NavController,
              public actionSheetCtrl: ActionSheetController,
              public platform: Platform,
              public noteService: NoteService,
              public translate: TranslateService,
              public popoverCtrl: PopoverController,
              public storage: Storage,) {}

  ngOnInit() {
    this.initNotes();
  }

  openItemOptionsPopover(note, noteElement: Ion) {
    let dataToPopover = {
      note: note,
      noteElement: noteElement._elementRef.nativeElement
    };
    this.itemOptionsPopover = this.popoverCtrl.create(ItemOptionsPopover, dataToPopover);

    this.itemOptionsPopover.onDidDismiss(isOpenNotificationPage => {
      if (isOpenNotificationPage)
        this.navCtrl.push(NotificationPage, {note: note})
    });

    this.itemOptionsPopover.present();
  }

  openListOptionsPopover(e) {
    this.popover = this.popoverCtrl.create(ListOptionsPopover);
    this.popover.onDidDismiss(data => {
      data ? this.isEnableSelectingMode = data.isEnableSelectingMode : this.isEnableSelectingMode = false;
    });
    this.popover.present({ev:e});
  }

  closeListOptionsPopover() {
    this.isEnableSelectingMode = false;
  }

  deleteSelectedItems() {
    let selectedNotes = [],
      selectedGeoNotes = [];
    this.notes.forEach(note => {
      if (note.selected) {
        note.content.lat ? selectedGeoNotes.push(note) : selectedNotes.push(note);
      }
    });
    this.noteService.removeArray(selectedNotes).then(() => {
      this.initNotes();
    });
    this.isEnableSelectingMode = false;
  }

  selectItem(e, note) {
    e.stopPropagation();
    note.selected = !note.selected;
    this.selected = this.notes.filter(e => e.selected).length;
  }

  openNote(note) {
    this.navCtrl.push(NotePage, {"note": note});
  }

  private initNotes() {
    this.noteService.getAll()
      .then(notes => this.notes = notes)
      .then(() => {
        this.notes.sort((v1, v2) => {
          return (v1.refreshed || v1.created).getTime() - (v2.refreshed || v2.created).getTime();
        })
      })
      .catch();
  }

  openMenu() {

    let translate = this.actionSheetTranslation();

    let actionSheet = this.actionSheetCtrl.create({
      title: translate.create,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: translate.note,
          icon: !this.platform.is('ios') ? 'document' : null,
          handler: () => {
            this.navCtrl.push(NotePage, {"note" : new Note()});
          }
        },
        {
          text: translate.cancel,
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
        }
      ]
    });
    actionSheet.present();
  }

  private actionSheetTranslation() {
    let translation = {
      create: "Create",
      cancel: "Cancel",
      note: "Note",
    };

    this.translate.get('CREATE').subscribe(val => translation.create = val);
    this.translate.get('CANCEL').subscribe(val => translation.cancel = val);
    this.translate.get('NOTE').subscribe(val => translation.note = val);

    return translation;
  }

  convertToImage(content) {
    let begin = content.lastIndexOf('{"image":');
    let src = content.slice(begin + 9);
    let end = content.indexOf('"}');
    return src.slice(0, end + 2);
  }

  setSrcOfImage(thumb, content) {
    thumb.src = this.convertToImage(content);
  }

  isImageExist(content) {
    return content.indexOf('{\"image\":') != -1;
  }
}
