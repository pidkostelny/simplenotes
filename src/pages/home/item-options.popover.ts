import {Component} from "@angular/core";
import {NoteService} from "../../shared/services/note.service";
import {NotificationsService} from "../../shared/services/notifications.service";
import {NavParams, ViewController} from "ionic-angular";

@Component({
  template: `
    <ion-title margin style="text-align: center">{{note && note.title}}</ion-title>
    <button ion-button color="dark" (click)="deleteNote()" clear block>{{'DELETE' | translate }}</button>
    <button [hidden]="note.reminderId" ion-button color="dark" (click)="setReminder()" clear block>{{'REMINDER' | translate }}</button>
    <button [hidden]="!note.reminderId" ion-button color="dark" (click)="cancelReminder()" clear block>{{'CANCEL_REMINDER' | translate }}</button>
  `
})
export class ItemOptionsPopover {

  note;
  noteElement: any;

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams,
              public noteService: NoteService,
              public notificationService: NotificationsService) {
    this.note = this.navParams.get('note');
    this.noteElement = this.navParams.get('noteElement');
  }

  setReminder() {
    this.viewCtrl.dismiss({isOpenNotificationPage: true});
  }

  cancelReminder() {
    return this.viewCtrl.dismiss().then(() => {
      this.notificationService.cancelNotification(this.note.reminderId).then(() => {
        this.note.reminderId = 0;
        return this.noteService.update(this.note);
      });
    })
  }

  deleteNote() {
    this.noteService.remove(this.note);
    this.noteElement.outerHTML = '';
    this.viewCtrl.dismiss();
  }
}
