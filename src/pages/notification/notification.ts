import * as moment from 'moment';
import {Component, OnInit} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {NotificationsService} from "../../shared/services/notifications.service";
import {Note} from "../../shared/classes/Note";
import {NoteService} from "../../shared/services/note.service";
import {NavController, NavParams} from "ionic-angular";
import {HomePage} from "../home/home";

@Component({
  selector: 'page-notification',
  templateUrl: './notification.html'
})
export class NotificationPage {

  minDate;
  maxDate = moment().year() + 10;
  monthNames;
  monthShortNames;
  dayNames;

  locale: moment.Locale;

  displayFormat;
  timeDisplayFormat;

  selectedDate;
  selectedTime;

  isRepeatEnable = false;
  repeat;

  note: Note;

  constructor(private translate: TranslateService,
              private notificationService: NotificationsService,
              private noteService: NoteService,
              private navParams: NavParams,
              private navCtrl: NavController){
    this.initDates();
    this.initLocale();
    this.initFormat();
    this.note = this.navParams.get("note");
  }

  changeFormat() {
    let now = moment(),
        selectedDate = moment(this.selectedDate);
    if (selectedDate.diff(now, "year", true) < 1) {
      this.displayFormat = this.locale.longDateFormat("ll");
    }
  }

  private initDates() {
    let now = moment().format();
    this.minDate = now;
    this.selectedDate = now;
    this.selectedTime = now;
  }

  private initLocale() {
    this.locale = moment.localeData(this.translate.getDefaultLang());
    this.monthNames = this.locale.months();
    this.monthShortNames = this.locale.monthsShort();
  }

  private initFormat() {
    let dateFormat = this.locale.longDateFormat("ll");
    dateFormat = dateFormat.slice(0, dateFormat.indexOf("YYYY"));
    while (dateFormat.match(/[\,\.]/)) {
      dateFormat = dateFormat.slice(0, -1);
    }
    this.displayFormat = dateFormat;
    this.timeDisplayFormat = this.locale.longDateFormat("LT");
  }

  save() {
    let time = moment(this.selectedTime),
        date = moment(this.selectedDate);
    date.hour(time.hour());
    date.minute(time.minute());
    this.navCtrl.popToRoot().then(() => {
      this.notificationService.setNotification(this.note, {date: date.toDate()})
          .then(id => {
            this.note.reminderId = id;
            this.noteService.update(this.note);
          })
      });
  }

}
