import {AfterViewInit, Component, ElementRef, OnInit} from "@angular/core";
import {MenuController, ViewController} from "ionic-angular";

declare let atrament: any;

const colors = [
  {color: '#32db64', name: 'secondary'},
  {color: '#F8FF4E', name: 'lemon'},
  {color: '#FFAA00', name: 'orange'},
  {color: '#f53d3d', name: 'danger'},
  {color: '#aa0867', name: 'jam'},
  {color: '#3914AF', name: 'blue'},
  {color: '#222222', name: 'dark'},
];


@Component({
  selector: 'sketch-modal',
  templateUrl: './sketch.html'
})
export class SketchModal implements OnInit, AfterViewInit {

  canvas;
  selectedColor;
  palette = colors;

  isEraserSet = false;

  height;
  width;


  constructor(private viewCtrl: ViewController,
              private menu: MenuController,
              private elRef: ElementRef) {}

  ngOnInit() {
    let modalEl = document.getElementsByTagName('sketch-modal')[0];
    this.width = modalEl.clientWidth;
    this.height = modalEl.clientHeight;
    this.canvas = atrament('#canvas', this.width, this.height);
    this.canvas.smoothing = false;
    this.canvas.adaptiveStroke = false;
    this.selectedColor = {color: '#488aff', name: 'primary'};
    this.canvas.color = this.selectedColor.color;
    this.canvas.weight = 5;
  }

  ngAfterViewInit() {
    this.menu.swipeEnable(false);
  }

  setColor(color) {
    this.palette.find((e, i) => {
      if (e.color == color.color) {
        this.palette[i] = this.selectedColor;
        this.selectedColor = color;
        let el: any = document.getElementsByClassName('fab-close-active')[0];
        el.click();
        this.canvas.color = color.color;
        return true;
      }
      return false;
    });

  }

  chooseTool() {
    if (this.isEraserSet) {
      this.canvas.weight = 5;
      this.canvas.mode = 'draw';
      this.isEraserSet = false;
    } else {
      this.canvas.weight = 20;
      this.canvas.mode = 'erase';
      this.isEraserSet = true;
    }
  }

  clearSketch() {
    this.canvas.clear();
  }

  dismiss() {
    this.viewCtrl.dismiss({sketch: this.canvas.toImage() })
  }
}
