
import {Component, ElementRef, AfterViewInit, OnInit,} from "@angular/core";


import * as Quill from 'quill';
import {TranslateService} from "@ngx-translate/core";
import {ModalController, NavController, NavParams} from "ionic-angular";
import {NoteService} from "../../shared/services/note.service";
import {Note} from "../../shared/classes/Note";
import {HomePage} from "../home/home";
import {SketchModal} from "./sketch/sketch.modal";


@Component({
  selector: 'page-note',
  templateUrl: './note.html'
})
export class NotePage implements AfterViewInit, OnInit {

  private textEditor: Quill;

  private addBulletListElement;
  private addOrderedListElement;
  private addImageElement;
  private isClicked: boolean = false;

  private note: Note;

  sketchModal;

  constructor(private translate: TranslateService,
              private noteService: NoteService,
              private elRef: ElementRef,
              private navParams: NavParams,
              private navCtrl: NavController,
              private modalCtrl: ModalController) {}

  ngOnInit() {
    this.note = this.navParams.get("note");
    this.initImagesWidth();
  }

  ngAfterViewInit() {
    let placeholder: string;
    this.translate.get('NOTE_GOES_HERE').subscribe(val => placeholder = val);
    this.textEditor = new Quill("#noteArea", {
      theme: 'bubble',
      placeholder: placeholder,
      modules: {
        history: {
          delay: 2000,
          maxStack: 50,
          userOnly: true
        },
        toolbar: [
          ['bold', 'italic', 'underline'],
          [{ 'color': [] }, { 'background': [] }],
          [{ 'align': [] }, { 'indent': '-1'}, { 'indent': '+1' }],
          [{ 'script': 'sub'}, { 'script': 'super' },
            { 'list': 'ordered'}, { 'list': 'bullet' }, 'image'],
        ]
      }
    });
    if (this.note.content) {
      this.textEditor.setContents(JSON.parse(this.note.content));
    }

    this.addOrderedListElement = this.elRef.nativeElement.querySelector('.ql-list[value=ordered]');
    this.addBulletListElement = this.elRef.nativeElement.querySelector('.ql-list[value=bullet]');
    this.addImageElement = this.elRef.nativeElement.querySelector('.ql-image');
  }

  addSketch() {
    this.sketchModal = this.modalCtrl.create(SketchModal);
    this.sketchModal.onDidDismiss(data => {
      let pos = this.textEditor.getLength();
      if (data) {
        this.textEditor.insertEmbed(pos, 'image', data.sketch);
      }
    });
    this.sketchModal.present();
  }

  addList() {
    this.addBulletListElement.click();
    // if(!this.isClicked && !this.elRef.nativeElement.querySelector('ol li')) {
    //   this.addBulletListElement.click();
    //   this.isClicked = true;
    // } else {
    //   this.addOrderedListElement.click();
    // }
    // setTimeout((function () {this.isClicked = false;}).bind(this), 400);
  }

  addImage() {
    this.addImageElement.click();
  }

  saveNote() {
    if (!this.note.content && !this.note.title) {
      this.navCtrl.setRoot(HomePage);
    }
    this.note.image = this.saveImageIfExist();
    this.note.content = JSON.stringify( this.textEditor.getContents());
    this.note.description = this.textEditor.getText(0, 200);
    (this.note.id ? this.noteService.update(this.note) : this.noteService.create(this.note)).then(() => {
      // this.navCtrl.setRoot(HomePage).then(() => this.adService.showInterstitial());
      this.navCtrl.setRoot(HomePage);
    });
  }

  private saveImageIfExist() {
    let image;
    this.textEditor.getContents().ops.forEach((k) => {
      k.insert.image && (image = k.insert.image);
    });
    return image;
  }

  private initImagesWidth() {
    let styles = document.createElement('style');
    styles.innerText = '.ql-editor img{max-height:' + window.innerHeight * 0.45 + 'px;' + 'width:auto;}';
    document.getElementsByTagName('body')[0].appendChild(styles);
  }
}
