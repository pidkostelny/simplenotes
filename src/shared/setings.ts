export const SETTINGS_KEY = "_settings";

export class Settings {
  language: string = "en-US";
  ledColor = "38B2CE";
}
