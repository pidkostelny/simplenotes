import * as moment from 'moment';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

const MS_IN_MONTH = 2592000000;
const MS_IN_WEEK  = 604800000;
const MS_IN_DAY   = 86400000;

@Pipe({
  name: 'localizedDate',
  pure: false
})
export class LocalizedDatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: string, pattern): any {
    let lang = this.translateService.getDefaultLang();
    let locale = moment.localeData(lang);
    let timeIntervalBetweenTodayCreation = new Date().getTime() - Date.parse(value);

    if(MS_IN_DAY > timeIntervalBetweenTodayCreation) {
      pattern = 'LT';
    } else if(MS_IN_WEEK > timeIntervalBetweenTodayCreation) {
      pattern = 'ddd';
    } else if(MS_IN_MONTH > timeIntervalBetweenTodayCreation) {
      pattern = this.getDayAndMonth(locale);
    } else {
      pattern = locale.longDateFormat('ll');
    }
    let time = moment(value);
    time.locale(lang);
    return time.format(pattern);
  }

  private getDayAndMonth(locale) {
    let dateFormat = locale.longDateFormat("ll");
    dateFormat = dateFormat.slice(0, dateFormat.indexOf("YYYY"));
    while (dateFormat.match(/[\,\.]/)) {
      dateFormat = dateFormat.slice(0, -1);
    }
    return dateFormat;
  }

}
