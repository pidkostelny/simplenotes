import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LanguageModal} from "../pages/settings/language.modal";
import {ItemOptionsPopover} from "../pages/home/item-options.popover";
import {ListOptionsPopover} from "../pages/home/list-options.popover";
import {TermsOfUsePage} from "../pages/terms-of-use/terms-of-use";
import {IonicStorageModule} from "@ionic/storage";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {PipesModule} from "../shared/pipes/pipes.module";
import {Network} from "@ionic-native/network";
import {NotePage} from "../pages/note/note";
import {NotificationPage} from "../pages/notification/notification";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {NotificationsService} from "../shared/services/notifications.service";
import {NoteService} from "../shared/services/note.service";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {Http, HttpModule} from "@angular/http";
import {SketchModal} from "../pages/note/sketch/sketch.modal";

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NotePage,
    NotificationPage,
    LanguageModal,
    ListOptionsPopover,
    ItemOptionsPopover,
    TermsOfUsePage,
    SketchModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'GeoNotesDB'
    }),
    PipesModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NotePage,
    NotificationPage,
    LanguageModal,
    ListOptionsPopover,
    ItemOptionsPopover,
    TermsOfUsePage,
    SketchModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NotificationsService,
    NoteService,
    LocalNotifications,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
