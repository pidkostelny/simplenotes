import {Component, OnInit, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import {langs} from "../assets/i18n/langs";
import {Settings, SETTINGS_KEY} from "../shared/setings";
import {TermsOfUsePage} from "../pages/terms-of-use/terms-of-use";
import {LanguageModal} from "../pages/settings/language.modal";
import {NOTIFICATION_KEY} from "../shared/services/notifications.service";
import {NotePage} from "../pages/note/note";
import {SketchModal} from "../pages/note/sketch/sketch.modal";
@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;

  settings: Settings = new Settings();

  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private storage: Storage,
              private translate: TranslateService) {}

  ngOnInit() {
    return this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.initStorage().then(() => {
        this.initTranslate();
      });
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  initTranslate() {
    this.translate.addLangs(langs);

    let platformLang =  this.platform.lang();

    this.storage.get('IS_FIRST_RUN_COMPLETED').then(val => {
      if (!val) {
        langs.find(v => {
          if (v.match(new RegExp(platformLang, 'gi'))) {
            this.settings.language = v;
            this.storage.set(SETTINGS_KEY, this.settings);
            return true;
          }
        });
        this.storage.set('IS_FIRST_RUN_COMPLETED', true);
      }
    });

    this.translate.setDefaultLang(this.settings.language);

    this.pages = [
      { title: "HOME", component: HomePage },
      { title: "LANGUAGE", component: LanguageModal},
      { title: "TERMS_OF_USE", component: TermsOfUsePage}
      // { title: "SETTINGS", component: SettingsPage }
    ];
  }

  initStorage() {
    return this.storage.ready()
      .then(() => {
        return this.storage.get(SETTINGS_KEY).then(val => {
          if (val) {
            this.settings = val;
          } else {
            this.storage.set(SETTINGS_KEY, new Settings());
          }
        });
      })
      .then(() => {
        return this.storage.get(NOTIFICATION_KEY).then(val => {
          if (!val) {
            val = 1;
          }
        })
      })
  }


}

